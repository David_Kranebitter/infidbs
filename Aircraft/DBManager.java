import java.awt.Dimension;
import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.TextAnchor;

public class DBManager {

	private static Connection c;
	private String pw = "Iquoxum321";
	private String databaseName = "aircraft";
	private Scanner sc = new Scanner(System.in);

	DBManager() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			c = DriverManager.getConnection("jdbc:mysql://localhost/" + databaseName, "root", pw);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Integer> getTimeStamp(int iTimeStamp) throws SQLException {
		String sql = "SELECT seentime FROM dump1090data";
		PreparedStatement stmt = null;
		ArrayList<Integer> list = new ArrayList<Integer>();

		Date date = new Date();
		try {
			stmt = c.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				if (rs.getInt(1) > date.getTime() / 1000 - iTimeStamp) {
					list.add(rs.getInt(1));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
		return list;

	}

	public ArrayList<String> getMilitaryFlights() {
		ArrayList<String> list = new ArrayList<String>();
		String sql = "Select flightnr from dump1090data where squawk > 5000 AND squawk < 6000 group by seentime desc limit 5";
		PreparedStatement stmt = null;
		try {
			stmt = c.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				list.add(rs.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public ArrayList<String> getSingleFlights() {
		ArrayList<String> list = new ArrayList<String>();
		String sql = "select flightnr, count(flightnr) as fs from dump1090data group by flightnr order by fs desc limit 3;";
		PreparedStatement stmt = null;
		try {
			stmt = c.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				list.add(rs.getString(1) + "   " + rs.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public int[] fillFlightArr(ArrayList<Integer> list) throws SQLException {
		int[] arr = new int[12];
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		for (int element : list) {
			Date date = Date.from(Instant.ofEpochSecond(element));
			String month = sdf.format(date);
			switch (month) {
			case "01":
				arr[0]++;
				break;
			case "02":
				arr[1]++;
				break;
			case "03":
				arr[2]++;
				break;
			case "04":
				arr[3]++;
				break;
			case "05":
				arr[4]++;
				break;
			case "06":
				arr[5]++;
				break;
			case "07":
				arr[6]++;
				break;
			case "08":
				arr[7]++;
				break;
			case "09":
				arr[8]++;
				break;
			case "10":
				arr[9]++;
				break;
			case "11":
				arr[10]++;
				break;
			case "12":
				arr[11]++;
				break;
			}
		}
		return arr;
	}

	@SuppressWarnings("deprecation")
	public JFreeChart plotBarChart(int[] arr) {

		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		dataset.addValue(arr[0], "Row 1", "J�nner");
		dataset.addValue(arr[1], "Row 2", "Februar");
		dataset.addValue(arr[2], "Row 3", "M�rz");
		dataset.addValue(arr[3], "Row 4", "April");
		dataset.addValue(arr[4], "Row 5", "Mai");
		dataset.addValue(arr[5], "Row 6", "Juni");
		dataset.addValue(arr[6], "Row 7", "Juli");
		dataset.addValue(arr[7], "Row 8", "August");
		dataset.addValue(arr[8], "Row 9", "September");
		dataset.addValue(arr[9], "Row 10", "Oktober");
		dataset.addValue(arr[10], "Row 11", "November");
		dataset.addValue(arr[11], "Row 12", "Dezember");

		JFreeChart chart = ChartFactory.createBarChart("Fl�ge in letzten 12 Monaten", "Monate", "Anzahl der Fl�ge",
				dataset, PlotOrientation.VERTICAL, true, true, false);
		

		BarRenderer renderer = (BarRenderer) chart.getCategoryPlot().getRenderer();
		renderer.setItemMargin(-5);
		renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		renderer.setBaseItemLabelsVisible(true);
		renderer.setItemLabelFont(new Font("Arial", 8, 15));
		ItemLabelPosition position = new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, TextAnchor.BASELINE_CENTER);
		renderer.setBasePositiveItemLabelPosition(position);

		return chart;
	}

	public JFreeChart plotPieChart(int[] arr) {
		DefaultPieDataset pieDataset = new DefaultPieDataset();
		pieDataset.setValue("J�nner", arr[0]);
		pieDataset.setValue("Februar", arr[1]);
		pieDataset.setValue("M�rz", arr[2]);
		pieDataset.setValue("April", arr[3]);
		pieDataset.setValue("Mai", arr[4]);
		pieDataset.setValue("Juni", arr[5]);
		pieDataset.setValue("Juli", arr[6]);
		pieDataset.setValue("August", arr[7]);
		pieDataset.setValue("Septmeber", arr[8]);
		pieDataset.setValue("Oktober", arr[9]);
		pieDataset.setValue("November", arr[10]);
		pieDataset.setValue("Dezember", arr[11]);

		JFreeChart chart = ChartFactory.createPieChart("Sample Pie Chart", pieDataset, true, false, false);

		return chart;
	}

}
