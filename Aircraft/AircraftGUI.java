import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import javax.swing.JTextArea;

public class AircraftGUI {

	private JFrame frmAircraftgui;
	private JPanel panel;
	private JButton btnStart;
	private DBManager dbm;

	private JButton btnPiechart;
	private ArrayList<String> list;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AircraftGUI window = new AircraftGUI();
					window.frmAircraftgui.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AircraftGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAircraftgui = new JFrame();
		frmAircraftgui.setTitle("AircraftGUI");
		frmAircraftgui.setBounds(100, 100, 480, 283);
		frmAircraftgui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAircraftgui.getContentPane().setLayout(null);

		panel = new JPanel();
		panel.setBounds(0, 0, 458, 227);
		frmAircraftgui.getContentPane().add(panel);
		panel.setLayout(null);

		btnStart = new JButton("Barchart");
		btnStart.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				try {
					dbm = new DBManager();
					int[] arr = dbm.fillFlightArr((dbm.getTimeStamp(toTimestamp("12"))));

					ChartFrame cFrm = new ChartFrame("Barchart", dbm.plotBarChart(arr));
					cFrm.pack();
					cFrm.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		btnStart.setBounds(15, 12, 115, 29);
		panel.add(btnStart);

		btnPiechart = new JButton("Piechart");
		btnPiechart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					dbm = new DBManager();
					int[] arr = dbm.fillFlightArr((dbm.getTimeStamp(toTimestamp("12"))));

					ChartFrame cFrm = new ChartFrame("Barchart", dbm.plotPieChart(arr));
					cFrm.pack();
					cFrm.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
		btnPiechart.setBounds(15, 51, 115, 29);
		panel.add(btnPiechart);

		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBounds(143, 82, 130, 129);
		panel.add(textArea);

		JTextArea textArea_1 = new JTextArea();
		textArea_1.setEditable(false);
		textArea_1.setBounds(295, 82, 115, 129);
		panel.add(textArea_1);

		JButton btnShowmilitary = new JButton("ShowMilitary");
		btnShowmilitary.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					list = new ArrayList<String>();

					dbm = new DBManager();
					list = dbm.getMilitaryFlights();
					for (String element : list) {
						textArea.append(element + "\n");
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnShowmilitary.setBounds(145, 36, 130, 29);
		panel.add(btnShowmilitary);

		JButton btnShowsingle = new JButton("ShowSingle");
		btnShowsingle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					list = new ArrayList<String>();
					dbm = new DBManager();
					list = dbm.getSingleFlights();
					for (String element : list) {
						textArea_1.append(element + "\n");
					}

				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		btnShowsingle.setBounds(295, 36, 115, 29);
		panel.add(btnShowsingle);
	}

	private int toTimestamp(String sMonth) {
		int iMonth = Integer.parseInt(sMonth);
		int iTimestamp = iMonth * 30 * 24 * 60 * 60;
		return iTimestamp;
	}
}
