import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class DBManager {

//	private static ArrayList<Temperature> temList = new ArrayList<Temperature>();
//	private static Connection c;
	private String pw = "Iquoxum321";
	private String databaseName = "Temperaturmessung";
	private Scanner sc = new Scanner(System.in);

	DBManager() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	
	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:mysql://localhost/" + databaseName, "root", pw);
	}

	public void createTable() throws SQLException {
		Connection c = getConnection();
		String sql = "";
		ArrayList<String> list = null;
		Statement stmt = null;
		Reader r = new Reader();
		try {
			stmt = c.createStatement();
			list = r.readIn();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < list.size(); i++) {

			if (!(list.get(i).contains(");"))) {
				sql += list.get(i);
			} else {
				sql += list.get(i);
				try {
					stmt.executeUpdate(sql);
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					if (stmt != null) {
						stmt.close();
					}
					if (c != null) {
						c.close();
					}
				}
				sql = "";

			}

		}

	}

	public ArrayList<Temperature> select(int limit) throws SQLException {
		Connection c = getConnection();
		String sql = "SELECT * FROM temperaturmessung LIMIT ?";
		PreparedStatement stmt = null;
		ArrayList<Temperature> list = new ArrayList<Temperature>();

		try {
			System.out.println("Limit: ");
			
			stmt = c.prepareStatement(sql);
			stmt.setInt(1, limit);
			sql = "SELECT * FROM temperaturmessung LIMIT " + limit;
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				list.add(new Temperature(rs.getInt(2), rs.getString(3)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (c != null) {
				c.close();
			}
		}
		return list;
	}

	public void insertInto(Temperature tem) throws SQLException {
		Connection c = getConnection();
		String sql = "INSERT INTO Temperaturmessung (Temperatur, Datum) VALUES (?,?)";
		PreparedStatement stmt = null;

		try {
			stmt = c.prepareStatement(sql);
			stmt.setInt(1, tem.getTemperature());
			stmt.setString(2, tem.getDateStr());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					if (stmt != null) {
						stmt.close();
					}

					if (c != null) {
						c.close();
					}
				}
			}
		}

	}

	public void menue() throws SQLException {

		while (true) {
			System.out.println("Welche Operation wollen sie ausf�hren?");
			System.out.println("Daten einf�gen ... 1");
			System.out.println("Daten auslesen ... 2");
			String ans = sc.next();
			switch (ans) {
			case "1":
				insertTem();
				break;
			case "2":
				showTem();
				break;
			default:
				System.out.println("Falsche Eingabe");
				break;
			}

		}
	}

	public void insertTem() throws SQLException {
		String temDate = "";
		int tem = 0;
		String temYear = "";
		String temMonth = "";
		String temDay = "";
		boolean errInput = false;
		boolean errTem = false;
		boolean errDate = false;
		do {
			System.out.println("Bitte Temperatur eingeben: ");
			try {
				errTem = false;
				tem = Integer.parseInt(sc.next());
			} catch (Exception e) {
				System.out.println("Falsche Eingabe");
				errTem = true;
			}
		} while (errTem);
		do {
			System.out.println("Datum angeben?: [J/N]");
			switch (sc.next()) {
			case "J":
				do {
				System.out.println("Bitte Datum eingeben");
				System.out.println("Jahr: ");
				temYear = sc.next();
				System.out.println("Monat");
				temMonth = sc.next();
				System.out.println("Day");
				temDay = sc.next();
				try {
					errDate = false;
					Integer.parseInt(temYear);
					Integer.parseInt(temMonth);
					Integer.parseInt(temDay);
				}catch(Exception e) {
					System.out.println("Falsche Eingabe");
					errDate = true;
				}
				
				}while(errDate);

				temDate = temYear + "-" + temMonth + "-" + temDay;
				System.out.println(temDate);
				errInput = false;
				break;
			case "N":
				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
				temDate = sdf.format(date);
				System.out.println(temDate);
				errInput = false;
				break;
			default:
				System.out.println("Falsche Eingabe");
				errInput = true;
				break;
			}
		} while (errInput);

		Temperature t = new Temperature(tem, temDate);
		insertInto(t);
	}

	public void showTem() throws SQLException {
		int limit = 0;
		boolean err = false;
		do {
		err = false;
		System.out.println("Limit: ");
		String sLimit = sc.next();
		try {
		limit = Integer.parseInt(sLimit);
		}catch(Exception e) {
			System.out.println("Falsche Eingabe");
			err = true;
		}
		}while(err);
		for (Temperature element : select(limit)) {
			System.out.println("-------------------------------------------------------------------------");
			System.out.println("Temperatur: " + element.getTemperature());
			System.out.println("Datum: " + element.getDateStr());
			System.out.println();
		}

	}

}
